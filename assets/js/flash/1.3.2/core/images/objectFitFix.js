/**
 * v 1.0
 */
flashCore.prototype.objectFitFix = function() {	
	if('objectFit' in document.documentElement.style === false) {
		document.querySelectorAll('.cover_image:not(.js_init--object_fit), .contain_image:not(.js_init--object_fit)').forEach(function(container){
			var img = container.querySelector('img');
			if(!img || img.classList.contains('lazyload')) {
				return;
			}
			container.classList.add('js_init--object_fit');
			var img_url = img.getAttribute('src');
			img.style.display = 'none';
			var img_div = document.createElement('div');
			img_div.classList.add('img');
			img.parentNode.appendChild(img_div);
			img_div.style.backgroundImage = 'url(\'' + img_url + '\')';
		});
	} else {
		document.querySelectorAll('.cover_image, .contain_image').forEach(function(el){
			el.innerHTML = el.innerHTML + '';
		});
	}
}