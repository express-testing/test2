/**
 * Binding a callback on page load, taking care of the execution status for
 * instantclick cache on back to history event. It's used to bind the js
 * for the scripts of the blocks.
 * @param  {Function} fn         	callback
 * @param  {string}   label 		the name of the block or a unique string associated to the scritp
 * @return {void}              
 */
flashCore.prototype.ready = function(fn, label, options) {
	var self = this;
	var defaults = {};
	if(typeof label === 'object') {
		options = Object.assign({}, label);
		label = null;
	}
	settings = Object.assign({callback: fn}, defaults, options);

	if(label) {
		self.startup_functions.named[label] = settings;
	} else {
		var key = Object.keys(self.startup_functions.anonymous).length;
		self.startup_functions.anonymous[key] = settings;
	}
}