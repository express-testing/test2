flashCore.prototype.compareBreakpoint = function(breakpoint, operators) {
    var self = this;
    var response = false;

    if(breakpoint.split(' ').length == 2) {
        var breakpoint_data = breakpoint.split(' ');
        breakpoint = breakpoint_data[0];

        switch(breakpoint_data[1]) {
            case 'up':
                operators = '>=';
                break;
            case 'down':
                operators = '<=';
                break;
            case 'only':
                operators = '=';
                break;
        }
    }

    if(!self.breakpoints[breakpoint]) {
        console.warn('There is no breakpoint named ' + breakpoint);
        return;
    }

    operators = operators.split('');
    operators.forEach( function(operator){
        if((!operator || operator === '=') && breakpoint === self.currentBreakpoint()) {
            response = true;
        } else if(operator === '>' && self.breakpoints[breakpoint] < window.innerWidth) {
            response = true;
        } else if(operator === '<' && self.breakpoints[breakpoint] > window.innerWidth) {
            response = true;
        }
    });

    return response;
}