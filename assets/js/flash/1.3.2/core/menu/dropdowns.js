/**
 * v 1.0
 * OPENING AND CLOSING DROPDOWN MENUS
 *
 * Usage:
 * 1) Add data-dropdown-target to the menu trigger and set a custom value (i.e. data-dropdown-target="mymenu1").
 * 2) Add data-dropdown with the same value as the trigger to the actual dropdown.
 * 3) If you want the dropdown to open also on hover you can add data-dropdown-hover to the trigger. 
 *
 * Example:
 * 	<a href="#" data-dropdown-target="my-menu-1" data-dropdown-hover>Menu Opener</a>
 *	<div data-dropdown="my-menu-1">
 *		<a href="/mypage">Link</a>
 *		<a href="/mypage2">Link</a> 
 *	</div>
 */

flashCore.prototype.dropdowns = function(){
	if(!document.body.classList.contains('js_init--dropdowns')) {
		document.body.classList.add('js_init--dropdowns');

		var currently_hovering = null; 		// Name of the hovered element
		var is_closing_menu = false;		// Is currently closing menus
		var opening_menu = null; 				// Name of the opening menu
		var dropdowns_hover_selector = ''; 
		var max_wating_time = 600; 			// Time waited before checking if the user is out of the menu item
		var hover_open_delay = 200;
		var close_timeout = null;
		var open_timeout = null;

		var dropdowns = document.querySelectorAll('[data-dropdown]');
		var dropdowns_with_hover = document.querySelectorAll('[data-dropdown-hover]');
		var dropdowns_triggers = document.querySelectorAll('[data-dropdown-target]');
		if(!dropdowns_triggers.length) {
			return;
		}

		/**
		 * SHARED METHODS FOR OPENING AND CLOSING
		 */
		
		// Opens dropdown
		function openDropdown(trigger) {
			opening_menu = trigger.getAttribute('data-dropdown-target');
			var dropdown = document.querySelector('[data-dropdown="' + opening_menu + '"]');

			// In case the dropdown doesn't exist
			if(!dropdown) {
				console.warn('The [data-dropdown="' + opening_menu + '"] dropdown does not exist');
				return;
			}

			// Closing other dropdowns
			var opened_menus = [];
			var all_opened_menus = dropdown.parentNode.parentNode.querySelectorAll('.open[data-dropdown-target]');
			all_opened_menus.forEach(function(menu){
				if(!menu.isSameNode(trigger)) {
					opened_menus.push(menu);
				}
			});

			if (!is_closing_menu) {
				is_closing_menu = true;
				clearTimeout(close_timeout);
				if(opened_menus.length) {
					opened_menus.forEach( function(dropdown){
						closeDropdown(dropdown);
					});
				}
				is_closing_menu = false;
			}

			// Show dropdown
			dropdown.style.display = 'block';	
			trigger.classList.add('open');	

			if(dropdown.classList.contains('megamenu')) {
				document.body.classList.add('overlay--megamenu');
			}
		};

		// Closes dropdown
		function closeDropdown(element) {
			var name = element.hasAttribute('data-dropdown') ? element.getAttribute('data-dropdown') : element.getAttribute('data-dropdown-target');
			var trigger = document.querySelector('[data-dropdown-target="' + name + '"]');

			// Close the dropdown
			var dropdown = document.querySelector('[data-dropdown="' + trigger.dataset.dropdownTarget + '"]');
			dropdown.style.display = 'none';
			
			// Remove the open class from the trigger
			trigger.classList.remove('open');	
			
			if(dropdown.classList.contains('megamenu')) {
				document.body.classList.remove('overlay--megamenu');
			}	
		};



		/**
		 * SETUP AND CLICK EVENTS
		 */
		// Hiding all the dropdowns
		for (var i = dropdowns.length - 1; i >= 0; i--) {
			dropdowns[i].style.display = 'none';
		}
		// Triggering menu open/close on click
		for (var i = dropdowns_triggers.length - 1; i >= 0; i--) {
			if(!dropdowns_triggers[i].hasAttribute('data-dropdown-hover')) {			
				flash.listen(dropdowns_triggers[i], 'click', function(e){
					e.preventDefault();

					if (e.target.classList.contains('open') ) {
						closeDropdown(e.target);
					} else {
						openDropdown(e.target);
					}
				});
			}
		}



		/**
		 * HOVER TRIGGER
		 */
		// Getting a list of selectors for hoverable drop down menus
		var dropdowns_hover = [];
		dropdowns_with_hover.forEach(function(el){
			var menu_name = el.getAttribute('data-dropdown-target');
			var this_menu = document.querySelector('[data-dropdown="' + menu_name + '"]');
			if(menu_name && this_menu) {
				dropdowns_hover.push('[data-dropdown="' + menu_name + '"]');
				this_menu.setAttribute('data-dropdown-hover','');
			}
			dropdowns_hover_selector = dropdowns_hover.join(',');
		});

		// In case there are hoverable dropdown menus
		if(dropdowns_hover_selector) {
			// Triggering menu open/close on hover intent
			var hover_triggers = document.querySelectorAll('[data-dropdown-hover]:not([data-dropdown])');
			flash.listen(hover_triggers, 'mouseenter', function(event){
				open_timeout = setTimeout(function(){
					openDropdown(event.target);
				}, hover_open_delay);
			});
			flash.listen(hover_triggers, 'mouseleave', function(event){
				var trigger = event.target;
				var name = trigger.dataset.dropdownTarget;

				clearTimeout(open_timeout);
				// Waits a little bit in case the user is moving from the link to the dropdown
				close_timeout = setTimeout(function(){
					if (currently_hovering !== name) {
						closeDropdown(trigger);
					}
				}, max_wating_time);
			});

			// Detecting name of current dropdown hovered (both trigger and actual menu)
			// so if a user goes from the link to the dropdown (and the opposite) the dropdown won't be closed
			var dropdowns_hover_filtered = document.querySelectorAll(dropdowns_hover_selector);
			flash.listen(dropdowns_hover_filtered, 'mouseenter', function(event){
				var menu = event.target;
				var name = menu.getAttribute('data-dropdown');
				currently_hovering = name;
				clearTimeout(close_timeout);
			});
			flash.listen(dropdowns_hover_filtered, 'mouseleave', function(event){	
				var menu = event.target;
				var name = menu.getAttribute('data-dropdown');
				currently_hovering = '';
			});

			// Detecting just dropdown hovering
			flash.listen(dropdowns, 'mouseleave', function(event){
				var menu = event.target;
				var name = menu.getAttribute('data-dropdown');
				// Waits a little bit in case the user is moving from the dropdown to the link
				close_timeout = setTimeout(function(){
					if (currently_hovering != name) {
						closeDropdown( menu );
					}
				}, max_wating_time);
			});
		}
	}
}