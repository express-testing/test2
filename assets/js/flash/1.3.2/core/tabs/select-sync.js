flashCore.prototype.selectSync = function(){
	var sync_select = document.querySelectorAll('[data-sync-select]');

	if(sync_select.length) {
		var prevent_refresh = false; // prevents select -> button -> select infinite loop

		sync_select.forEach( function(select){

			var settings = select.getAttribute('data-sync-select').split('|');
			var links_wrapper = document.querySelector(settings[0]); // The buttons list
			var links = document.querySelectorAll('[' + settings[1] + ']');

			// If settings are not correct, the function just returns
			if(settings.length != 2 || !links_wrapper || links.length == 0) {
				return;
			}

			setTimeout(function(){
				prevent_refresh = true;
				if(links_wrapper.querySelector('.active[' + settings[1] + ']')) {
					var item_value = links_wrapper.querySelector('.active[' + settings[1] + ']').getAttribute(settings[1]);
					if(select.querySelector('option[value="' + item_value + '"]')) {
						select.value = item_value;
					}
				}
				prevent_refresh = false;
			}, 50);	

			// Sync select --> buttons
			select.addEventListener('change', function(event){
				if ( prevent_refresh ) {
					return;
				}

				var current_sector = event.target.value;
				if(settings[1] == 'href') {
                    Turbolinks.visit(current_sector);
                } else {
                    document.querySelector('[' + settings[1] + '="' + current_sector + '"]').click();
                }
			});

			// Sync buttons --> select
			flash.listen(links_wrapper, 'click', function(){
				var current_sector = event.target.getAttribute(settings[1]);
				prevent_refresh = true;
				select.value = current_sector;
				prevent_refresh = false;
			}, '[' + settings[1] + ']');
		});
	}
}